#! /usr/bin/env node

const fs = require("fs-extra");
const chokidar = require("chokidar");

const [, , ...argv] = process.argv;

const args = {
  cacheClean: (argv.includes("cache", 0) && argv.includes("clean", 1)) || false,
  watch: argv.includes("watch") === true || false
};

let settings = { paths: ["node_modules/"], usePolling: true };

try {
  const userSettings = require(process.cwd() + "/package.json")[
    "sass-silencer"
  ];
  settings = { ...settings, ...userSettings };
} catch (err) {
  throw err;
}

const writeFile = data => {
  fs.writeFile(data.bundleFile, data.content, err => {
    if (err) throw err;
    console.log(`${data.watcherFile} has been silénced.`);
  });
};

const readFile = data => {
  fs.readFile(data.bundleFile, "utf8", (err, rows) => {
    if (err) throw err;
    const regex = /(?<=@?)(\b.*)?(\B\.[a-zA-Z0-9-_]+)/g;
    const content = rows.replace(regex, (match, $1, $2) => {
      if ($1 === undefined || $1.includes("extend")) {
        return match.replace($2, $2.replace(".", "%"));
      } else {
        return match;
      }
    });
    writeFile({ ...data, ...{ content } });
  });
};

const handleData = data => {
  const split = data.watcherFile.split("/");
  const filePath = `${split.slice(split.indexOf(data.silentDir)).join("/")}`;
  const bundleFile = `${__dirname}/bundle/${filePath}`;
  if (data.event === "change") {
    try {
      fs.removeSync(bundleFile);
    } catch (err) {
      console.error(err);
    }
  }
  try {
    fs.copySync(data.watcherFile, bundleFile);
  } catch (err) {
    console.error(err);
  }
  readFile({ ...data, ...{ bundleFile, filePath } });
};

const createWatcher = data => {
  const watcher = chokidar.watch(
    process.cwd() + "/" + data.silentPath + "**/*.scss",
    {
      ignored: /(^|[\/\\])\../,
      usePolling: settings.usePolling,
      persistent: args.watch
    }
  );
  watcher.on("all", (event, file) => {
    handleData({ ...data, ...{ watcherFile: file, event } });
  });
};

const main = () => {
  if (args.cacheClean) {
    fs.removeSync(`${__dirname}/bundle/`);
  }
  settings.paths.map(path => {
    const split = path.split("/");
    const data = {
      bundlePath: `${__dirname}/bundle`,
      silentPath: path,
      silentDir:
        split.slice(-1)[0] === "" ? split.slice(-2)[0] : split.slice(-1)[0]
    };
    createWatcher(data);
  });
};

main();
