# Sass Silencer

This package helps you to write more modular SCSS by making inheritance more flexible. Sass Silencer basically collects SCSS-files from your project, including third party Sass libraries. From these files, it creates a "silenced" bundle, which means that every class selector will be converted in to a placeholder. This allows you to flexibly import any styles you'd like to inherit from. You can just simply `@extend %placeholder`.

Sass Silencer is mainly designed to be used with CSS Modules, but there's no reason you couldn't use it in a regular Sass project.

## Why?

The @import feature of Sass is great if you just need to split your CSS into multiple files. However, since web sites are getting more modular thanks to some modular web frameworks and libraries, it could be a good idea to write styles in a modular way too. This is possible with CSS Modules. By combining CSS Modules and Sass, we can do inheritance between components. There is just one problem; every file gets compiled as many times as they get imported. This means duplicates in the final render. Also, imagine a scenario where you import a big Sass library to your single SCSS-module just to extend one class -> the whole library gets modularized by CSS modules. That can be a huge performance issue.

Since, Sass's placeholders won't get compiled or modularized, placeholders are a great way to do base level styling. However, not much Sass libraries provides a placeholder-only version. At this point Sass Silencer comes handy.

## Usage

Install:

```bash
$ npm install sass-silencer --save
```

Set SASS_PATH environment variable to point Sass Silencer's bundle directory. This tells node-sass where to look for .scss files -> makes your imports work.

```
SASS_PATH=node_modules/sass-silencer/bundle
```

Open package.json and edit your start script by adding `sass-silencer` like this:

```json
"scripts": {
  "start": "sass-silencer watch | react-scripts start",
  "build": "sass-silencer cache clean && react-scripts build"
}
```

In package.json, define what directories will be watched by Sass Silencer. If path not set, `"node_modules/"` will be watched by default, which can be heavy for regular use.

```json
"sass-silencer": {
  "paths": [
    "node_modules/bootstrap-scss/",
    "src/styles/"
  ]
}
```

## Options and commands

Include paths:

```javascript
"paths": [
  "node_modules/" // default
]
```

Chokidar's usePolling:

```javascript
"usePolling": true // default
```

One time silence:

```
$ sass-silencer
```

Remove bundle directory and resilence:

```
$ sass-silencer cache clean
```

Watch for file changes:

```
$ sass-silencer watch
```
